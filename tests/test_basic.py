import sys
import os
import unittest

sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from app import create_app  # Убедитесь, что этот импорт теперь работает


class BasicTestCase(unittest.TestCase):

    def test_example(self):
        self.assertTrue(True)


if __name__ == '__main__':
    unittest.main()
