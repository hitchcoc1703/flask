# app/routes.py

from flask import Blueprint, request, jsonify

bp = Blueprint('main', __name__)


@bp.route('/')
def index():
    return jsonify({"message": "Hello, World!"})


@bp.route('/echo', methods=['POST'])
def echo():
    data = request.json
    return jsonify(data)
